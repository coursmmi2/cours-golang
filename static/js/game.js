let table;
let tds;
let pTurn;
let otherTurn;
let winner;
let link;

var socket = io();
var gameSocket = io("/game/");


function play(event) {
    if(!event.target.classList.contains("prevent")){
        event.target.innerText = players[playerUuid];
        event.target.classList.add("prevent");
        removeClickHandler();
        let pos = event.target.cellIndex+'-'+event.target.parentNode.rowIndex;
        let playerIndex = currentPlayerIndex === 0 ? 1 : 0;
        gameSocket.emit('sendMove', `{"gameUuid": "${gameUuid}", "movePos": "${pos}", "playerIndex":"${playerIndex}"}`, function(){});

        checkWin(event.target);
    }
}

function removeClickHandler(){
    pTurn.classList.add("hidden");
    otherTurn.classList.remove("hidden");

    [...tds].forEach((td) => {
        td.removeEventListener("click", play);
    });
}

function addClickHandler(){
    pTurn.classList.remove("hidden");
    otherTurn.classList.add("hidden");

    [...tds].forEach((td) => {
        td.addEventListener("click", play);
    });
}

function updateTab(pos, playerIndex){
    let x = parseInt(pos.split("-")[0]);
    let y = parseInt(pos.split("-")[1]);
    let td = table.getElementsByTagName("tr")[y].getElementsByTagName("td")[x];
    playerIndex = playerIndex === 0 ? 1 : 0;

    td.innerText = Object.values(players)[playerIndex];
    td.classList.add("prevent");
    checkWin(td);
}

function checkWin(td){

    var rowTd = td.parentElement.getElementsByTagName("td");
    var cols = table.querySelectorAll(`tr td:nth-child(${td.cellIndex+1})`);

    var diag1 = table.querySelectorAll(`            
        tr:nth-child(3) td:nth-child(1) ,
        tr:nth-child(2) td:nth-child(2) ,
        tr:nth-child(1) td:nth-child(3)
    `);

    var diag2 = table.querySelectorAll(`            
        tr:nth-child(1) td:nth-child(1) ,
        tr:nth-child(2) td:nth-child(2) ,
        tr:nth-child(3) td:nth-child(3)
    `);

    var i = count(rowTd,0);
    var j = count(cols,0);
    var k = count(diag1,0);
    var l = count(diag2,0);

    function count(tab, count){
        [...tab].forEach(function(oneTd, index){
            if(tab[index-1] && oneTd.innerText === tab[index-1].innerText  ){
                if( Object.keys(symbols).indexOf(oneTd.innerText) > -1  ){
                    count++;
                }
            }
        });
        return count;
    }

    console.log(i, j, k, l);

    if(i === 2 || j === 2){
        console.log("sendWinner col/row");
        let winnerUuid = symbols[td.innerText];
        var string = `{"winnerUuid": "${winnerUuid}", "gameUuid": "${gameUuid}" }`;
        console.log(string)
        gameSocket.emit('sendWinner', string, function(){});
    } else if ( k === 2 || l === 2 ){
        console.log("sendWinner diag");
        let winnerUuid = symbols[table.rows[1].cells[1].innerText];
        var string = `{"winnerUuid": "${winnerUuid}", "gameUuid": "${gameUuid}" }`;
        console.log(string);
        gameSocket.emit('sendWinner', string, function(){});
    }

}

window.addEventListener("load", function () {
    table = document.getElementsByTagName("table")[0];
    pTurn = document.getElementById("pTurn");
    otherTurn = document.getElementById("otherTurn");
    winner = document.getElementById("winner");
    link = document.getElementById("link");

    tds = table.querySelectorAll("td");
    if(playerUuid === playerOneUuid){
        let url = "http://localhost:7070?gameUuid="+gameUuid;
        link.innerHTML += `<a target="_blank" href=${url}> ${url}</a>`;
        link.classList.remove("hidden");

        gameSocket.emit('joinroom', gameUuid, function(){});

        // Wait other player
        console.log("setListener");
        gameSocket.on('gamehasstarted', function(msg){
            link.classList.add("hidden");
            console.log("gameHasStarted", msg);
            addClickHandler();
        });

    } else{
        // Tell other player that game has started
        gameSocket.emit('startgame', gameUuid, function(){});

        otherTurn.classList.remove("hidden");
    }

    gameSocket.on('turnToPlay', function(msg){
        console.log("turnToPlay", msg);
        let response = JSON.parse(msg);
        if(Object.keys(players)[response.playerIndex] === playerUuid){
            addClickHandler();
            updateTab(response.movePos, parseInt(response.playerIndex));
        }
    });

    gameSocket.on('endGame', function(msg){
        setTimeout(function(){
            console.log("endGame", msg);
            pTurn.classList.add("hidden");
            otherTurn.classList.add("hidden");
            winner.innerText = "The winner is the player with symbol : "+players[msg];
        },500)

    });

})