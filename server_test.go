package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	//"net/http"
	//"net/http/httptest"
	"testing"
)

func TestGameRoute(t *testing.T) {
	fmt.Println("starting test")

	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(GameRoute)
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

}
