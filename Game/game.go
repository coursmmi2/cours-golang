package Game

type Game struct {
	GameUuid      string
	PlayerOneUuid string
	PlayerTwoUuid string
	CurrentPlayer int
}
