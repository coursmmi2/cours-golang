package main

import (
	"crypto/rand"
	"encoding/json"
	"fmt"
	"github.com/googollee/go-socket.io"
	"html/template"
	"log"
	"net/http"
	"tp_go/Game"
)

var games = make(map[string]Game.Game)
var chann = make(chan Game.Game)

type Move struct {
	GameUuid    string `json:"gameUuid"`
	MovePos     string `json:"movePos"`
	PlayerIndex string `json:"playerIndex"`
}

type Winner struct {
	WinnerUuid string `json:"winnerUuid"`
	GameUuid   string `json:"gameUuid"`
}

func main() {
	fmt.Println("Starting server port: 7070")

	http.HandleFunc("/", GameRoute)

	fs := http.FileServer(http.Dir("./static"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))

	// Creating io server
	socket, err := socketio.NewServer(nil)

	socket.OnConnect("/", func(s socketio.Conn) error {
		s.SetContext("")
		fmt.Println("[socket connected]:", s.ID())
		return nil
	})

	socket.OnEvent("/game/", "startgame", func(s socketio.Conn, msg string) {
		fmt.Println("[socket]/game/ startgame", msg)
		s.Join(msg)
		s.SetContext(msg)
		socket.BroadcastToRoom("/game/", msg, "gamehasstarted", msg)
	})

	socket.OnEvent("/game/", "joinroom", func(s socketio.Conn, msg string) {
		fmt.Println("[socket] /game/ startgame", msg)
		s.Join(msg)
	})

	socket.OnEvent("/game/", "sendMove", func(s socketio.Conn, msg string) {
		fmt.Println("[socket] sendMove", msg)
		var move Move
		json.Unmarshal([]byte(msg), &move)
		fmt.Println("id", move.GameUuid, err)

		socket.BroadcastToRoom("/game/", move.GameUuid, "turnToPlay", msg)

	})

	socket.OnEvent("/game/", "sendWinner", func(s socketio.Conn, msg string) {
		fmt.Println("[socket] sendWinner", msg)
		var winner Winner
		err = json.Unmarshal([]byte(msg), &winner)
		fmt.Println("winner", winner.GameUuid, err)

		socket.BroadcastToRoom("/game/", winner.GameUuid, "endGame", winner.WinnerUuid)

	})

	go socket.Serve()

	go updateGameMap()

	http.Handle("/socket.io/", socket)

	log.Fatal(http.ListenAndServe(":7070", nil))

}

func generateUuid() string {
	b := make([]byte, 16)
	_, err := rand.Read(b)
	if err != nil {
		log.Fatal(err)
	}

	uuid := fmt.Sprintf("%x-%x-%x-%x-%x", b[0:4], b[4:6], b[6:8], b[8:10], b[10:])

	return uuid
}

func GameRoute(w http.ResponseWriter, r *http.Request) {
	fmt.Println("[gameroute] /game/")
	params, ok := r.URL.Query()["gameUuid"]
	// second player
	if ok {
		fmt.Println("ok")
		game, ok := games[params[0]]
		if ok {
			fmt.Println("[gameroute] secondPlayer")

			game.CurrentPlayer = 1

			t, _ := template.ParseFiles("./template/index.html")
			t.Execute(w, game)

		}
	} else {
		fmt.Println("[gameroute] firstPlayer")
		// first player
		// game doesn't exist
		gameUuid := generateUuid()
		PlayerOneUuid := generateUuid()
		game := Game.Game{GameUuid: gameUuid, PlayerOneUuid: PlayerOneUuid, PlayerTwoUuid: generateUuid(), CurrentPlayer: 0}

		go func() {
			chann <- game
		}()

		t, _ := template.ParseFiles("./template/index.html")
		t.Execute(w, game)
	}
}

func updateGameMap() {
	newGame := <-chann
	games[newGame.GameUuid] = newGame
}
